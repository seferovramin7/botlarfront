export default {
  setAuthed(state, payload) {
    state.authed = payload
  },
  setUserDetails(state, payload) {
    state.userDetails = payload
  },
  setGlobalMessage(state, globalMessage) {
    state.globalMessage = globalMessage
  },
  setInputErrorMessage(state, globalMessage) {
    state.inputErrorMessage = globalMessage
  },
  setDocType(state, payload) {
    state.docType = payload
  },
  setCalcResult(state, payload) {
    state.calcResult = payload;
  },
  setIsModalOpen(state, payload) {
    state.isModalOpen = payload
  },
  setAppId(state, payload) {
    state.appId = payload;
  },
  setApplication(state, payload) {
    state.application = payload;
  },
  setForm(state, payload) {
    state.form = payload;
  },
}