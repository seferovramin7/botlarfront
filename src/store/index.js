import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'
import mutations from './mutations'
import getters from './getters'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    authed: false,
    userDetails: {},
    globalMessage: {
      text: '',
      active: false
    },
    inputErrorMessage: {
      text: '',
      active: false
    },
    docType: 1,
    calcResult:{
      amount: null,
      month: null,
      percent: null,
      amountPerMonth: null,
    },
    isModalOpen: false,
    appId: null,
    application: {},
    form: {
      phoneNumber: "",
      relationOne: "",
      relationTypeOne: "",
      relationOneNumber: "",
      relationTwo: "",
      relationTypeTwo: "",
      relationTwoNumber: "",
      paymentWay: "",
    },
  },
  mutations: mutations,
  actions: actions,
  getters: getters,
  modules: {
  },
})
