export default {
  updateAuthed({ commit }, payload) {
    commit('setAuthed', payload)
  },
  updateUserDetails({ commit }, payload) {
    commit('setUserDetails', payload)
  },
  updateDocType({ commit }, payload) {
    commit('setDocType', payload)
  },
  updateGlobalMessage({ commit }, payload) {
    commit('setGlobalMessage', payload)
  },
  updateInputErrorMessage({ commit }, payload) {
    commit('setInputErrorMessage', payload)
  },
  updateCalcResult( {commit}, payload) {
    commit('setCalcResult', payload);
  },
  updateIsModalOpen( {commit}, payload) {
    commit('setIsModalOpen', payload)
  },
  updateAppId({commit}, payload) {
    commit('setAppId', payload);
  },
  updateApplication({commit}, payload) {
    commit('setApplication', payload);
  },
  updateForm({commit, state}, payload) {
    const form = {...state.form};
    form[payload.property] = payload.value;
    commit('setForm', form);
  }
}