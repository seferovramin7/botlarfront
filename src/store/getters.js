export default {
    getIsModalOpen(state) {
        return state.isModalOpen;
    }
}