import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "@/views/Home";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  // {
  //   path: '/main',
  //   name: 'Main',
  //   meta: { auth: true },
  //   component: Main,
  //   redirect: '/documents',
  //   children: [
  //     {
  //       path: '/documents',
  //       name: 'Documents',
  //       meta: { auth: true },
  //       component: () => import(/* webpackChunkName: "Documents" */ '../views/Documents.vue')
  //     },
  //     {
  //       path: '/pending',
  //       name: 'Pending',
  //       meta: { auth: true },
  //       component: () => import(/* webpackChunkName: "Pending" */ '../views/Pending.vue')
  //     },
  //     {
  //       path: '/signed',
  //       name: 'Signed',
  //       meta: { auth: true },
  //       component: () => import(/* webpackChunkName: "Signed" */ '../views/Signed.vue')
  //     },
  //     {
  //       path: '/sign',
  //       name: 'Sign',
  //       meta: { auth: true },
  //       component: () => import(/* webpackChunkName: "Sign" */ '../views/Sign.vue')
  //     },
  //     {
  //       path: '/offers',
  //       name: 'offers',
  //       meta: {auth: true},
  //       component: () => import(/* webpackChunkName: "Offers" */ '../views/Offers'),
  //     },
  //     {
  //       path: '/approve',
  //       name: 'approve',
  //       meta: {auth: true},
  //       component: () => import(/* webpackChunkName: "Approve Offer" */ '../views/Approve'),
  //     },
  //     {
  //       path: '/approved',
  //       name: 'approved',
  //       meta: {auth: true},
  //       component: () => import(/* webpackChunkName: "Approved Offer" */ '../views/Approved'),
  //     },
  //   ]
  // },
  //
  // {
  //   path: '/login',
  //   name: 'Login',
  //   component: () => import(/* webpackChunkName: "Login" */ '../views/Login.vue')
  // },

  {
    path: "*",
    component: () => import(/* webpackChunkName: "NotFound" */ '../views/NotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,

  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return {
        selector: to.hash
      }
    }
    return {
      x: 0,
      y: 0
    }
  }
})

export default router
