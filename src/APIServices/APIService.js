import axios from 'axios';
import store from '@/store/index'
import router from '@/router/index'


import { SiteSettings } from "./SiteSettings";

export default {
    ...SiteSettings
}

axios.interceptors.request.use(
    config => {
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use((response) => response, (error) => {
    throw error;
});