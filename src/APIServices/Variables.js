let domain = (new URL(window.location.href));
domain = domain.hostname;
let BASE_URL_v
if (domain == 'localhost') {
    BASE_URL_v = 'http://localhost:8080'
} else {
    BASE_URL_v = '/'
}

export const BASE_URL = BASE_URL_v;

